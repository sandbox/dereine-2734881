<?php

namespace Drupal\Tests\entity_reference_views\Functional;

use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests the autocreate views selection plugin.
 * @group entity_reference_views
 *
 * @see \Drupal\entity_reference_views\Plugin\EntityReferenceSelection\AutocreateViewsSelection
 */
class AutocreateViewsSelectionTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = ['views', 'entity_reference_views', 'entity_test', 'taxonomy', 'entity_reference_views_test'];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $tags = Vocabulary::create([
      'name' => 'tags',
      'vid' => 'tags',
    ]);
    $tags->save();

    FieldStorageConfig::create([
      'entity_type' => 'entity_test',
      'type' => 'entity_reference',
      'field_name' => 'field_test_reference',
      'settings' => [
        'target_type' => 'taxonomy_term',
      ],
    ])->save();

    FieldConfig::create([
      'entity_type' => 'entity_test',
      'bundle' => 'entity_test',
      'field_name' => 'field_test_reference',
      'settings' => [
        'handler' => 'views_autocreate',
        'handler_settings' => [
          'auto_create' => TRUE,
          'target_bundles' => ['tags' => 'tags'],
          'view' => [
            'view_name' => 'entity_reference_views',
            'display_name' => 'entity_reference_1',
            'arguments' => [],
          ],
        ],
      ],
    ])->save();

    $form_display = entity_get_form_display('entity_test', 'entity_test', 'default');
    $form_display->setComponent('field_test_reference', [
      'type' => 'entity_reference_autocomplete',
    ]);
    $form_display->save();

    $user = $this->drupalCreateUser(['administer entity_test content']);
    $this->drupalLogin($user);
  }

  /**
   * Tests the autocreate views selection
   */
  public function testForm() {
    $this->drupalGet('/entity_test/add');
    $this->assertSession()->statusCodeEquals(200);
    file_put_contents('/tmp/debug.txt', $this->getSession()->getPage()->getContent());
    $this->submitForm(['field_test_reference[0][target_id]' => 'foo'], t('Save'));

    $entity = EntityTest::load(1);
    $this->assertEquals('foo', $entity->field_test_reference->entity->label());
    $this->assertEquals('tags', $entity->field_test_reference->entity->bundle());
  }

}
